CC		= gcc
CFLAGS	= -Wall -Wextra -Werror -pedantic -std=c99 -Wno-write-strings \
		  -Wno-unused-parameter -Wshadow -Wpointer-arith -Wcast-qual \
          -Wstrict-prototypes -Wmissing-prototypes
SRCDIR	= src
OBJDIR	= build
TARGET	= mile

SRCEXT	= c
SOURCES	= $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS	= $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

.PHONY: clean, fresh

$(TARGET): $(OBJECTS)
	$(CC) $^ -o $(TARGET)

$(OBJDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(OBJDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) -r $(OBJDIR) $(TARGET)

fresh: clean $(TARGET)

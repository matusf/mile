# Mile Text Editor
[![pipeline status](https://gitlab.com/matusf/mile/badges/master/pipeline.svg)](https://gitlab.com/matusf/mile/commits/master)

Terminal text editor build from scratch in c.


## Build
To build `mile` simply run make.
```
$ make
```


## Usage
Usage is pretty straight forward. Run `mile` with an argument to open a file. To create a new one, run it without arguments.
```
$ mile <path-to-file>
$ mile
```


### Features
Working features in this versions are:
- `ctrl+s` to save file (note: when editing a new buffer you will be prompted to enter the filename. to abort saving hit `esc`)
- `crtl+q` to quit editing (note: you will be prompted to push it 2 more times if a file is not saved)
- `page-up` & `page-down` keyboard shortcuts
- `home` & `end` keyboard shortcuts
- move via `arrow keys`
- `ctrl+h`, `del` or `backspace` to delete character

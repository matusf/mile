#ifndef EDITOR_H
#define EDITOR_H


#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

#include "data.h"
#include "logging.h"

void die(const char *s);
void disable_raw_mode(void);
void enable_raw_mode(void);
int editor_read_key(void);
int get_cursor_position(int *rows, int *cols);
int get_window_size(int *rows, int *cols);
void editor_init(void);
void editor_insert_row(int at, char *s, size_t len);
void editor_update_row(editor_row *row);
void editor_set_status_message(const char *fmt, ...);
void editor_row_insert_char(editor_row *row, int at, int c);
void editor_insert_char(int c);
void editor_row_del_char(editor_row *row, int at);
void editor_del_char(void);
void editor_free_row(editor_row *row);
void editor_del_row(int at);
void editor_row_append_string(editor_row *row, char *s, size_t len);
void editor_insert_newline(void);

#endif

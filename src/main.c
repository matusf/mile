#include "data.h"
#include "editor.h"
#include "io.h"


int main(int argc, char *argv[]) {
    enable_raw_mode();
    editor_init();
    if (argc >= 2) {
        editor_open(argv[1]);
    }

    editor_set_status_message(
        "HELP: ctrl-s = save | ctrl-q = quit | ctrl-f = find");

    while (1) {
        editor_refresh_screen();
        editor_process_keypress();
    }

    return 0;
}

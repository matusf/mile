#ifndef APPEND_BUFFER_H
#define APPEND_BUFFER_H

#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define ABUF_INIT {NULL, 0}

struct abuf {
    char *buff;
    int len;
};

void abuf_append(struct abuf *ab, const char *s, int len);
void abuf_free(struct abuf *ab);

#endif

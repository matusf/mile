#ifndef IO_H
#define IO_H

#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "append_buffer.h"
#include "data.h"
#include "editor.h"
#include "logging.h"
#include "search.h"

#define CTRL_KEY(k) ((k) & 0x1f)

void editor_scroll(void);
void editor_draw_rows(struct abuf *ab);
void editor_refresh_screen(void);
void editor_move_cursor(int key);
void editor_process_keypress(void);
void editor_open(char *filename);
int editor_cx_to_rx(editor_row *row, int cx);
int editor_rx_to_cx(editor_row *row, int rx);
void editor_draw_status_bar(struct abuf *ab);
void editor_draw_message_bar(struct abuf *ab);
char *editor_rows_to_string(int *buff_len);
void editor_save(void);
char *editor_prompt(char *prompt);

#endif

#include "append_buffer.h"

void abuf_append(struct abuf *ab, const char *s, int len) {
    char *new = realloc(ab->buff, ab->len + len);

    if (new == NULL) return;
    memcpy(&new[ab->len], s, len);
    ab->buff = new;
    ab->len += len;
}

void abuf_free(struct abuf *ab) {
    free(ab->buff);
}

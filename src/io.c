#include "io.h"

void editor_scroll() {
    E.rx = 0;
    if (E.cy < E.num_rows) {
        E.rx = editor_cx_to_rx(&E.row[E.cy], E.cx);
    }

    if (E.cy < E.row_off) {
        E.row_off = E.cy;
    }
    if (E.cy >= E.row_off + E.screen_rows) {
        E.row_off = E.cy - E.screen_rows + 1;
    }
    if (E.rx < E.col_off) {
        E.col_off = E.rx;
    }
    if (E.rx >= E.col_off + E.screen_cols) {
        E.col_off = E.rx - E.screen_cols + 1;
    }
}

void editor_draw_rows(struct abuf *ab) {
    int r;
    for (r = 0; r < E.screen_rows; r++) {
        int file_row = r + E.row_off;
        if (file_row >= E.num_rows) {
            // Welcome message only when no file is opened
            if (E.num_rows == 0 && r == E.screen_rows / 3) {
                char welcome[80];
                int welcome_len = snprintf(
                    welcome,
                    sizeof(welcome),
                    "Mile editor -- version %s",
                    MILE_VERSION
                );
                if (welcome_len > E.screen_cols) welcome_len = E.screen_cols;
                int padding = (E.screen_cols - welcome_len) / 2;
                if (padding) {
                    abuf_append(ab, "~", 1);
                    padding--;
                }
                while (padding--) abuf_append(ab, " ", 1);
                abuf_append(ab, welcome, welcome_len);
            } else {
                abuf_append(ab, "~", 1);
            }
        } else {
            int len = E.row[file_row].render_size - E.col_off;
            if (len < 0) len = 0;
            if (len > E.screen_cols) len = E.screen_cols;
            abuf_append(ab, &E.row[file_row].render[E.col_off], len);
        }

        // Erase line
        abuf_append(ab, "\x1b[K", 3);
        abuf_append(ab, "\r\n", 2);
    }
}

void editor_refresh_screen() {
    editor_scroll();

    struct abuf ab = ABUF_INIT;
    // Hide cursor
    abuf_append(&ab, "\x1b[?25l", 6);
    abuf_append(&ab, "\x1b[H", 3);

    editor_draw_rows(&ab);
    editor_draw_status_bar(&ab);
    editor_draw_message_bar(&ab);

    char buff[32];
    // Set cursor
    snprintf(buff, sizeof(buff), "\x1b[%d;%dH",
            (E.cy - E.row_off) + 1, (E.rx - E.col_off) + 1);
    abuf_append(&ab, buff, strlen(buff));

    // Show cursor
    abuf_append(&ab, "\x1b[?25h", 6);

    write(STDIN_FILENO, ab.buff, ab.len);
    abuf_free(&ab);
}

void editor_move_cursor(int key) {
    // Check is cursor is on valid line (not last one)
    editor_row *row = (E.cy >= E.num_rows) ? NULL : &E.row[E.cy];

    switch (key) {
        case ARROW_LEFT:
            if (E.cx != 0) {
                E.cx--;
            } else if (E.cy > 0) {
                E.cy--;
                E.cx = E.row[E.cy].size;
            }
            break;
        case ARROW_RIGHT:
            // Do not owerflow to the right
            if (row && E.cx < row->size) {
                E.cx++;
            } else if (row && E.cx == row->size) {
                E.cy++;
                E.cx = 0;
            }
            break;
        case ARROW_UP:
            if (E.cy != 0) {
                E.cy--;
            }
            break;
        case ARROW_DOWN:
            if (E.cy < E.num_rows - 1) {
                E.cy++;
            }
            break;
    }

    row = (E.cy >= E.num_rows) ? NULL : &E.row[E.cy];
    int row_len = row ? row->size : 0;
    if (E.cx > row_len) {
        E.cx = row_len;
    }
}

void editor_process_keypress() {
    static int quit_times = MILE_QUIT_TIMES;
    int c = editor_read_key();

    switch (c) {
        case '\r':
            editor_insert_newline();
            break;

        case CTRL_KEY('q'):
            if (E.dirty && quit_times > 0) {
                editor_set_status_message("Warning! Unsaved changes. "
                    "Press ctrl-q %d more times to quit,", quit_times
                );
                quit_times--;
                return;
            }
            write(STDIN_FILENO, "\x1b[2J", 4);
            write(STDIN_FILENO, "\x1b[H", 3);
            exit(0);
            break;

        case CTRL_KEY('s'):
            editor_save();
            break;

        case HOME_KEY:
            E.cx = 0;
            break;

        case END_KEY:
            if (E.cy < E.num_rows) {
                E.cx = E.row[E.cy].size;
            }
            break;

        case CTRL_KEY('f'):
            editor_search();
            break;

        case BACKSPACE:
        case CTRL_KEY('h'):
        case DEL_KEY:
            // Not allow delete when on last char in last row
            if (c == DEL_KEY && E.cy == E.num_rows -1 && E.cx == E.row[E.cy].size)
                return;
            if (c == DEL_KEY)
                editor_move_cursor(ARROW_RIGHT);
            editor_del_char();
            break;

        case PAGE_UP:
        case PAGE_DOWN:
            {
                if (c == PAGE_UP) {
                    E.cy = E.row_off;
                } else if (c == PAGE_DOWN) {
                    E.cy = E.row_off + E.screen_rows - 1;
                    if (E.cy > E.num_rows) {
                        E.cy = E.num_rows - 1;
                    }
                }
                int times = E.screen_rows;
                while (times--) {
                    editor_move_cursor(c == PAGE_UP ? ARROW_UP : ARROW_DOWN);
                }
            }
            break;

        case ARROW_UP:
        case ARROW_DOWN:
        case ARROW_LEFT:
        case ARROW_RIGHT:
            editor_move_cursor(c);
            break;

        // Ignore unwanted control sequences
        case CTRL_KEY('l'):
        case '\x1b':
            break;

        default:
            editor_insert_char(c);
            break;
    }

    quit_times = MILE_QUIT_TIMES;
}

void editor_open(char *filename) {
    free(E.filename);
    E.filename = strdup(filename);

    FILE *fp = fopen(filename, "r");
    if (!fp) die("fopen");

    char *line = NULL;
    size_t line_cap = 0;
    ssize_t line_len;

    while ((line_len = getline(&line, &line_cap, fp)) != -1) {
        // Strip trailing whitespace
        while (line_len > 0 &&
              (line[line_len - 1] == '\n' ||
               line[line_len - 1] == '\r')) {
            line_len--;
        }
        editor_insert_row(E.num_rows, line, line_len);
    }

    free(line);
    fclose(fp);
    E.dirty = 0;
}

int editor_cx_to_rx(editor_row *row, int cx) {
    int rx = 0;
    for (int c = 0; c < cx; c++) {
        if (row->chars[c] == '\t') {
            rx += (MILE_TAB_STOP - 1) - (rx % MILE_TAB_STOP);
        }
        rx++;
    }
    return rx;
}

int editor_rx_to_cx(editor_row *row, int rx) {
    int cur_rx = 0;
    int cx;
    for (cx = 0; cx < row->size; cx++) {
        if (row->chars[cx] == '\t') {
            cur_rx += (MILE_TAB_STOP - 1) - (cur_rx % MILE_TAB_STOP);
        }
        cur_rx++;
        if (cur_rx > rx) return cx;
    }
    return cx;
}

void editor_draw_status_bar(struct abuf *ab) {
    // Invert colors
    abuf_append(ab, "\x1b[7m", 4);
    char status[80], rstatus[80];
    int len = snprintf(status, sizeof(status), "%.20s - %d lines %s",
        E.filename ? E.filename : "[no filename]", E.num_rows,
        E.dirty ? "(modified)" : ""
    );
    int rlen = snprintf(rstatus, sizeof(rstatus), "%d/%d",
        E.cy + 1, E.num_rows);

    if (len > E.screen_cols) {
        len = E.screen_cols;
    }
    abuf_append(ab, status, len);

    while (len < E.screen_cols) {
        if (E.screen_cols - len == rlen) {
            abuf_append(ab, rstatus, rlen);
            break;
        } else {
            abuf_append(ab, " ", 1);
            len++;
        }
    }
    // Invert colors back
    abuf_append(ab, "\x1b[m", 3);
    abuf_append(ab, "\r\n", 2);
}

void editor_draw_message_bar(struct abuf *ab) {
    // Clear line
    abuf_append(ab, "\x1b[K", 3);
    int msg_len = strlen(E.statusmsg);
    if (msg_len > E.screen_cols) msg_len = E.screen_cols;
    if (msg_len && time(NULL) - E.statusmsg_time < 5) {
        abuf_append(ab, E.statusmsg, msg_len);
    }
}

char *editor_rows_to_string(int *buff_len) {
    int total_len = 0;
    for (int r = 0; r < E.num_rows; r++) {
        total_len += E.row[r].size + 1;
    }
    *buff_len = total_len;

    char *buff = malloc(total_len);
    char *tmp = buff;
    for (int r = 0; r < E.num_rows; r++) {
        memcpy(tmp, E.row[r].chars, E.row[r].size);
        tmp += E.row[r].size;
        *tmp = '\n';
        tmp++;
    }

    return buff;
}

void editor_save() {
    if (E.filename == NULL) {
        E.filename = editor_prompt("Save as: %s");
        if (E.filename == NULL) {
            editor_set_status_message("Save aborted");
            return;
        }
    }

    int len;
    char *buff = editor_rows_to_string(&len);

    int fd = open(E.filename, O_RDWR | O_CREAT, 0644);
    if (fd != -1) {
        if (ftruncate(fd, len) != -1) {
           if (write(fd, buff, len) == len) {
               close(fd);
               free(buff);
               E.dirty = 0;
               editor_set_status_message("%d bytes written to disk", len);
               return;
           }
        }
        close(fd);
    }
    free(buff);
    editor_set_status_message("Can't save! I/O error : %s", strerror(errno));
}

char *editor_prompt(char *prompt) {
    size_t buff_size = 128;
    char *buff = malloc(buff_size);

    size_t buff_len = 0;
    buff[0] = 0;

    while (1) {
        editor_set_status_message(prompt, buff);
        editor_refresh_screen();

        int c = editor_read_key();
        if (c == DEL_KEY || c == BACKSPACE || c == CTRL_KEY('h')) {
            if (buff_len != 0) buff[--buff_len] = 0;
        } else if (c == '\x1b') {
            editor_set_status_message("");
            free(buff);
            return NULL;
        } else if (c == '\r') {
            if (buff_len != 0) {
                editor_set_status_message("");
                return buff;
            }
        } else if (!iscntrl(c) && c < 128) {
            if (buff_len == buff_size - 1) {
                buff_size *= 2;
                buff = realloc(buff, buff_size);
            }
            buff[buff_len++] = c;
            buff[buff_len] = 0;
        }
    }
}

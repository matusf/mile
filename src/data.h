#ifndef DATA_H
#define DATA_H

#include <termios.h>
#include <time.h>

#define MILE_VERSION "0.0.1"
#define MILE_TAB_STOP 8
#define MILE_QUIT_TIMES 2

typedef struct editor_row {
    int size;
    int render_size;
    char *chars;
    char *render;
} editor_row;

struct editor_config {
    struct termios orig_termios;
    int screen_rows;
    int screen_cols;
    int cx;
    int cy;
    int rx;
    int row_off;
    int col_off;
    int num_rows;
    int dirty;
    char *filename;
    char statusmsg[80];
    time_t statusmsg_time;
    editor_row *row;
};

struct editor_config E;

enum editor_key {
    BACKSPACE = 127,
    // Large initial value, not to conflict with char
    ARROW_LEFT = 1000,
    ARROW_RIGHT,
    ARROW_UP,
    ARROW_DOWN,
    DEL_KEY,
    HOME_KEY,
    END_KEY,
    PAGE_UP,
    PAGE_DOWN
};

#endif

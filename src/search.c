#include "search.h"

void editor_search(void) {
    char *query = editor_prompt("Search: %s (ESC to cancel)");
    if (query == NULL) return;

    for (int i = 0; i < E.num_rows; i++) {
        editor_row *row = &E.row[i];
        char *match = strstr(row->render, query);
        if (match) {
            E.cy = i;
            E.cx = editor_rx_to_cx(row, match - row->render);
            E.row_off = E.num_rows;
            break;
        }
    }

    free(query);
}
